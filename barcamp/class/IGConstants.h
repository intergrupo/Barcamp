//
//  IGConstants.h
//  barcamp
//
//  Created by Jhon Jaiver López Calderón on 7/19/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#ifndef IG_Constants_h
#define IG_Constants_h

#define API_PLACES @"http://barcampmed.azurewebsites.net/api/programacion"
#define API_UNCONFERENCE @"http://barcampmed.azurewebsites.net/api/Conferencias"
#define API_TWITTER_QUERY @"http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=20&q=https://twitter.com/statuses/user_timeline/36675597.rss"

#define PLACES_KEY @"places"
#define UNCONFERENCES_KEY @"unconferences"
#define TWITTS_KEY @"entries"

#define BARCAMP_URL @"http://barcampmedellin.org/"
#define BARCAMP_TWITTER @"@barcamp_med"

#define FONT_SIZE 13.0f
#define CELL_CONTENT_WIDTH 295.0f
#define CELL_CONTENT_MARGIN 10.0f


#endif

@interface IGConstants : NSObject

@end
